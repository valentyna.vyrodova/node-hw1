// requires...

const fs = require('fs');
const { log } = require('console')
const path = require('path');

// constants...
const FILE_PATH = './files/';
const extensions = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];



function createFile(req, res, next) {
    // Your code to create the file.

    const fileExtensions = path.extname(req.body.filename);

    if (!extensions.includes(fileExtensions)) {
        next({
            status: 400,
            message: 'Applications supports log, txt, json, yaml, xml, js file extensions'
        });
        return;
    }

    if (req.body.content.length === 0) {
        next({
            status: 400,
            message: 'Content parameter are not specify'
        });
        return;
    }

    fs.writeFile(FILE_PATH + req.body.filename, req.body.content, (err) => {
        if (err) {
            next({
                status: 400,
                message: err.message
            });
            return;
        }
        res.status(200).send({ "message": "File created successfully" });
    })


}

function getFiles(req, res, next) {
    // Your code to get all files.
    fs.readdir(FILE_PATH, (err, files) => {
        if (err) {
            next({
                status: 400,
                message: 'Client error'
            });
            return;
        }

        res.status(200).send({
            message: 'Success',
            files: files,
        });
    });
}



const getFile = (req, res, next) => {
    // Your code to get all files.
    let filename = req.url.slice(1);

    fs.readFile(FILE_PATH + filename, 'utf-8', (err, data) => {
        if (err) {
            next({
                status: 400,
                message: `No file with ${filename} found`
            });
            return
        }

        res.status(200).send({
            message: "Success",
            filename: path.basename(FILE_PATH + filename),
            content: data,
            extension: path.extname(filename).slice(1),
            uploadedDate: fs.statSync(FILE_PATH + filename).birthtime
        });
    });
}


// Other functions - editFile, deleteFile

const editFile = (req, res, next) => {
    let filename = req.url.slice(1);

    if (!fs.existsSync(FILE_PATH + filename)) {
        next({
            status: 400,
            message: `No file with ${filename} found`
        });
        return;
    }

    fs.appendFile(FILE_PATH + filename, updatedText, (err) => {
        if (err) {
            next({
                status: 400,
                message: `Something went wrong`,
            });
            return;
        }
        res.status(200).send({
            message: 'Success saving',
        });
    })
}

const deleteFile = (req, res, next) => {
    let filename = req.url.slice(1);

    if (!fs.existsSync(FILE_PATH + filename)) {
        next({
            status: 400,
            message: `No file with ${filename} found`
        });
    }

    fs.unlink(FILE_PATH + filename, (err) => {
        if (err) {
            next({
                status: 400,
                message: `Something went wrong`,
            });
            return;
        }
        res.status(200).send({
            message: 'Success delete',
        });
    })
}

module.exports = {
    createFile,
    getFiles,
    getFile,
    editFile,
    deleteFile
}